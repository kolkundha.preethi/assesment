package com.example.demo;
import java.util.*;
	public class savings {
		private String OwnerName;
		private List<Address> address = new ArrayList<>();
		private int BalanceAmount;
		private int WithdrwalAmount;
		private long Date;
		private List<Status> status = new ArrayList<>();

		public savings(String ownerName, List<Address> address, int balanceAmount, long date, List<Status> status) {
			OwnerName = ownerName;
			this.address = address;
			BalanceAmount = balanceAmount;
			Date = date;
			this.status = status;
		}

	    public String getOwnerName() {
			return OwnerName;
		}

		public void setOwnerName(String ownerName) {
			OwnerName = ownerName;
		}

		public List<Address> getAddress() {
			return address;
		}

		public void setAddress(List<Address> address) {
			this.address = address;
		}

		public int getBalanceAmount() {
			return BalanceAmount;
		}

		public void setBalanceAmount(int balanceAmount) {
			BalanceAmount = balanceAmount;
		}

		public int getWithdrwalAmount() {
			return WithdrwalAmount;
		}

		public void setWithdrwalAmount(int withdrwalAmount) {
			WithdrwalAmount = withdrwalAmount;
		}

		public long getDate() {
			return Date;
		}

		public void setDate(long date) {
			Date = date;
		}

		public List<Status> getStatus() {
			return status;
		}

		public void setStatus(List<Status> status) {
			this.status = status;
		}

		int deposit(int BalanceAmount) {
			int amount = this.getBalanceAmount();
			return amount;
		}

		int Withdrawl(int BalanceAmount, int WithdrwalAmount) {
			int TotalAmount = this.BalanceAmount;
			int withdrawlAmount = this.WithdrwalAmount;
			if (withdrawlAmount > TotalAmount)
				return withdrawlAmount;
			return 0;
		}
	}

	
