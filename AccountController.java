package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/Account")
@RestController
public class AccountController {
	
	@Autowired
	AccountService accountService;
	@GetMapping("/")
    void getAccounts() {
	accountService.getAccounts();
   	System.out.println("Account Holders Details");
	}
	@GetMapping("/{id}")
    void getUserDetails(@PathVariable Integer id) {
   	System.out.println("Account Number  "+id);
	}
	@PostMapping("/")
	 String ownerDetails(@RequestBody Account account) {
		accountService.save(account);
		System.out.println("Owner name: "+account.getOwnerName());
		System.out.println("Residing City Name : "+account.getAddress_City());
		System.out.println("State: "+account.getState());
		System.out.println("City Pin: "+account.getPin());
		System.out.println("Balance Amount in Account: "+account.getBalanceAmount());
		System.out.println("Date of Creation: "+account.getDoc());
		 return "postcalled";
	}
	
}
