package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {//POJO
	@Autowired
	UserService userService;//dependency
	@GetMapping("/")
     void getUsers() {
    	System.out.println("Called");
    	
    }
	@GetMapping("/{id}")
    void getUser(@PathVariable Integer id) {
   	System.out.println("Called  "+id);
   	
   }
	@PostMapping("/")
	 String saveUser(@RequestBody User user) {//to  create user
		userService.save(user);
		System.out.println("got user "+user.getName());
		System.out.println("got user age "+user.getAge());
		return "postcalled";
	}
	@PutMapping("/")
	String handlePUtMapping() {
		System.out.println("put");
		return "put method called";
		
		
	}
	
}
