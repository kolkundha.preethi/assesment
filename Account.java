package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Account_Number;
	String OwnerName;
	String Address_City;
	String State;
	int pin;
	int BalanceAmount;
	int doc;

	public Integer getAccount_Number() {
		return Account_Number;
	}
	public void setAccount_Number(Integer account_Number) {
		Account_Number = account_Number;
	}
	public String getOwnerName() {
		return OwnerName;
	}
	public void setOwnerName(String ownerName) {
		OwnerName = ownerName;
	}
	public String getAddress_City() {
		return Address_City;
	}
	public void setAddress_City(String address_City) {
		Address_City = address_City;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public int getDoc() {
		return doc;
	}
	public void setDoc(int doc) {
		this.doc = doc;
	}
	public int getBalanceAmount() {
		return BalanceAmount;
	}
	public void setBalanceAmount(int balanceAmount) {
		BalanceAmount = balanceAmount;
	}
	

}
